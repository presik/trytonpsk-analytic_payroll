# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class StaffSection(metaclass=PoolMeta):
    "Staff Section"
    __name__ = 'staff.section'
    analytic = fields.Many2One('analytic_account.account', 'Analytic Account',
                               domain=[('type', '=', 'normal')])
