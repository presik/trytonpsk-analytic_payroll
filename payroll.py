# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.pool import PoolMeta, Pool
from trytond.modules.analytic_account import AnalyticMixin
from trytond.pyson import Eval, If
from .exceptions import AnalyticPayrollError
from trytond.i18n import gettext
from trytond.wizard import Wizard, StateTransition
from trytond.transaction import Transaction


STATES = {
        'readonly': Eval('state') != 'draft',
}


class PayrollLine(AnalyticMixin, metaclass=PoolMeta):
    __name__ = "staff.payroll.line"

    @classmethod
    def __setup__(cls):
        super(PayrollLine, cls).__setup__()
        cls.analytic_accounts.domain = [
            ('company', '=', If(~Eval('_parent_payroll'),
                    Eval('context', {}).get('company', -1),
                    Eval('_parent_payroll', {}).get('company', -1))),
            ]

    def get_analytic_lines(self, account, line, date):
        "Yield analytic lines for the accounting line and the date"
        lines = []
        amount = line['debit'] or line['credit']
        for account, amount in account.distribute(amount):
            analytic_line = {}
            analytic_line['debit'] = amount if line['debit'] else Decimal(0)
            analytic_line['credit'] = amount if line['credit'] else Decimal(0)
            analytic_line['account'] = account
            analytic_line['date'] = date
            lines.append(analytic_line)
        return lines

    def _get_entry(self, line_move):
        if self.analytic_accounts:
            line_move['analytic_lines'] = []
            to_create = []
            for entry in self.analytic_accounts:
                if not entry.account:
                    continue
                # Just debits must to create entries, credits not
                if not entry.account or line_move['debit'] == 0:
                    continue
                to_create.extend(self.get_analytic_lines(entry.account, line_move, self.payroll.date_effective))
            if to_create:
                line_move['analytic_lines'] = [('create', to_create)]
        return line_move

    def get_move_line(self, account, party, amount):
        line = super(PayrollLine, self).get_move_line(account, party, amount)
        line_ = self._get_entry(line)
        return line_


class PayrollAddAnalytic(Wizard):
    'Payroll Add Analytic'
    __name__ = 'analytic_payroll.add.analytic'
    start_state = 'add_analytic'
    add_analytic = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PayrollAddAnalytic, cls).__setup__()

    def transition_add_analytic(self):
        Payroll = Pool().get('staff.payroll')
        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'
        payrolls = Payroll.browse(ids)

        for p in payrolls:
            p.set_analytic_line()
        return 'end'


class Payroll(metaclass=PoolMeta):
    __name__ = "staff.payroll"

    @classmethod
    def __setup__(cls):
        super(Payroll, cls).__setup__()

    def set_preliquidation(self, config, extras, discounts=None, cache_wage_dict=None):
        super(Payroll, self).set_preliquidation(config, extras, discounts,cache_wage_dict)
        group_analytic = {mw.wage_type.id: mw.analytic_account.id
            for mw in self.employee.mandatory_wages if mw.analytic_account}
        employee = self.employee.rec_name
        for line in self.lines:
            wage_id = line.wage_type.id
            if wage_id not in group_analytic.keys():
                continue
            for acc in line.analytic_accounts:
                try:
                    acc.write([acc], {'account': group_analytic[wage_id]})
                except:
                    wage = line.wage_type.rec_name
                    raise AnalyticPayrollError(
                        gettext('analytic_payroll.msg_error_on_analytic_employee', employee=employee, wage=wage)
                    )
        self.save()
    
    def set_analytic_line(self):
        group_analytic = {mw.wage_type.id: mw.analytic_account.id
            for mw in self.employee.mandatory_wages if mw.analytic_account}
        if group_analytic:
            option = list(group_analytic.keys())[0]
            for line in self.lines:
                key = line.wage_type.id
                if key not in list(group_analytic.keys()):
                    for acc in line.analytic_accounts:
                        acc.write([acc], {'account': group_analytic[option]})
                else:
                    for acc in line.analytic_accounts:
                        acc.write([acc], {'account': group_analytic[key]})
            self.save()

    def get_moves_lines(self, wage_dict):
        move_lines = super(Payroll, self).get_moves_lines(wage_dict)
        for ml in move_lines:
            if ml['analytic_lines']:
                ml['analytic_lines'][0][1][0]['debit']= ml['debit']
                ml['analytic_lines'][0][1][0]['credit']= ml['credit']
        return move_lines


class PayrollMoveAnalytic(Wizard):
    'Payroll Move Analytic'
    __name__ = 'analytic_payroll.move.analytic'
    start_state = 'move_analytic'
    move_analytic = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PayrollMoveAnalytic, cls).__setup__()

    def transition_move_analytic(self):
        Payroll = Pool().get('staff.payroll')
        Analytic = Pool().get('analytic_account.line')
        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'
        payrolls = Payroll.browse(ids)

        for p in payrolls:
            accounts = {w.wage_type.debit_account: w.analytic_account for w in p.employee.mandatory_wages}
            analytics = []
            analytic_append = analytics.append
            for l in p.move.lines:
                if not l.analytic_lines and l.account.code >= '4' \
                    and l.account in list(accounts.keys()):
                    value = {
                        'account': accounts[l.account],
                        'move_line': l.id,
                        'debit': l.debit,
                        'credit': l.credit,
                    }
                    analytic_append(value)
            Analytic.create(analytics)
        return 'end'
